﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 6/3/2016
 * Time: 10:16 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using System.Net.Mail;


namespace AutoTemplateLinking
{
	/// <summary>
	/// Description of ToolsClass.
	/// </summary>
	public partial class ToolsClass : Form
	{
		public ToolsClass()
		{
			
		}
		
		public static string GetConnection()
		{
			string DataSource = ToolsClass.getConfig ("DataSource",false,"","Config.xml");	
			string UserID = ToolsClass.getConfig ("UserID",false,"","Config.xml");	
			string Password = ToolsClass.getConfig ("Password",false,"","Config.xml");	
			string DBName = ToolsClass.getConfig ("DBName",false,"","Config.xml");	
			string ConCsiDB = "Data Source="+DataSource+";Initial Catalog="+DBName+";User ID="+UserID+";Password="+Password;
			return ConCsiDB;
		}
		public static string csimes_SapInterface()
		{
			string DataSource = ToolsClass.getConfig ("DataSource",false,"","Config.xml");	
			string UserID = ToolsClass.getConfig ("UserID",false,"","Config.xml");	
			string Password = ToolsClass.getConfig ("Password",false,"","Config.xml");	
			string DBName = "csimes_SapInterface";
			string ConCsiDB = "Data Source="+DataSource+";Initial Catalog="+DBName+";User ID="+UserID+";Password="+Password;
			return ConCsiDB;
		}
		public static string CentralizedDatabase()
		{
			string DataSource = ToolsClass.getConfig ("DataSource",false,"","Config.xml");	
			string UserID = ToolsClass.getConfig ("UserID",false,"","Config.xml");	
			string Password = ToolsClass.getConfig ("Password",false,"","Config.xml");	
			string DBName = "CentralizedDatabase";	
			string ConCsiDB = "Data Source="+DataSource+";Initial Catalog="+DBName+";User ID="+UserID+";Password="+Password;
			return ConCsiDB;
		}
		
		 public static SqlDataReader GetDataReader(string SqlStr,string sDBConn="")
        {
            SqlDataReader dr = null;
            string ConnStr = "";
            if(string.IsNullOrEmpty(sDBConn))
            	ConnStr =  GetConnection();
            else
            ConnStr = sDBConn;
         
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConnStr);
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = SqlStr;
                cmd.CommandType = CommandType.Text;
                try
                {
                    if (conn.State == ConnectionState.Closed) 
                        conn.Open();
                     dr = cmd.ExecuteReader();//System.Data.CommandBehavior.SingleResult
                   
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    dr = null;
                }
               
            }
            return dr;
        }

		 public static DataTable PullData(string query,string connString = "")
    	{
		 	if(string.IsNullOrEmpty(connString))
		 		connString = GetConnection();
		 	
		 	try{
		    		DataTable dt = new DataTable();
        			SqlConnection conn = new SqlConnection(connString);        
      				SqlCommand cmd = new SqlCommand(query, conn);
       				conn.Open();

       				 // create data adapter
        			SqlDataAdapter da = new SqlDataAdapter(cmd);
        			// this will query your database and return the result to your datatable
       			 	da.Fill(dt);
        			conn.Close();
        			da.Dispose();
        			return dt;
		    	}
		    	catch (Exception e)
		    	{
		    		ToolsClass.WriteErrorLog(query,e.Message,"");
		    		return null;
		    	}
		    
		
    	}
		 public static int PutsData(string query,string connString = "")
   		{
		    if(string.IsNullOrEmpty(connString))
		 		connString = GetConnection();		
		 	try{
		    		SqlConnection conn = new SqlConnection(connString);
		    		conn.Open();
   					SqlCommand myCommand = new SqlCommand(query,conn);

   					int i = myCommand.ExecuteNonQuery();   					
   					conn.Close();
   					return i;
		    	}
		    	catch (Exception ex)
		    	{
		    		ToolsClass.WriteErrorLog(query,ex.Message,"");
		    		return 0;
		    	}
    	}
		 public static string getConfig(string sNodeName, bool isAttribute = false, string sAttributeName = "", string configFile = "")//(int CartonQty, int IdealPower, int MixCount, int PrintMode, string ModuleLabelPath, string CartonLabelPath, string CartonLabelParameter, string CheckRule)
        {
            string svalue = "";
            if (configFile == "" || configFile == null)
                configFile = "config.xml";
            string xmlFile = AppDomain.CurrentDomain.BaseDirectory + "\\" + configFile;
            if (!System.IO.File.Exists(xmlFile))
            {    
            	return "";
            }
            try
            {
                XmlReader reader = XmlReader.Create(xmlFile);
                if (reader.ReadToFollowing(sNodeName))
                {
                    if (!isAttribute)
                        svalue = reader.ReadElementContentAsString();
                    else
                        svalue = reader.GetAttribute(sAttributeName);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
              	System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            	return "";
            }
            if (svalue != null)
                svalue = svalue.Replace('\r', ' ').Replace('\n', ' ').Trim();
            else
                svalue = "";
            return svalue;
        }
		 
		 
//		 public static XmlNode subNode(XmlDocument doc, XmlNode ParentNode, string sNodeName, string sValue, string sAttributeName = "",string sAttribute = "")
//        {
//            XmlNode subNode = doc.CreateElement(sNodeName);
//            if (sAttribute != "")
//            {
//                XmlAttribute subAttribute = doc.CreateAttribute(sAttributeName);
//                subAttribute.Value = sAttribute;
//                subNode.Attributes.Append(subAttribute);
//            }
//            if (sValue != "")
//                subNode.AppendChild(doc.CreateTextNode(sValue));
//            ParentNode.AppendChild(subNode);
//            return subNode;
//        }
		 

		  public static void WritetoFile(string C,string template, string FilePath = "")
		  {
		  	if(FilePath == "")
		  		FilePath = Application.StartupPath + "\\Log.txt";
		  	
		  	File.AppendAllText(FilePath, Environment.NewLine + C + "	 " + template + "	 " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
		  }
		  
		  public static void WriteErrorLog(string C,string template,string error)
		  {
//		  	string FilePath = Application.StartupPath + "\\ErrorLog.txt";
//		  	File.AppendAllText(FilePath, Environment.NewLine + C + "	 " + template + "	 " + error+ "	 " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));		  
		  		StreamWriter sw = null;
	            try
	            {
	                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\ErrorLog.txt", true);
	                sw.WriteLine(GetTimestamp(DateTime.Now) + ": " + C + "	" + template + "	" + error);
	                sw.Flush();
	                sw.Close();
	            }
	            catch
	            {
	            }
		  
		  }
		  
	        public static void WriteToLog(string C,string template)
	        {
	            StreamWriter sw = null;
	            try
	            {
	                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory +"\\Log.txt", true);
	                sw.WriteLine(GetTimestamp(DateTime.Now) + ": " + C + "	" + template);
	                sw.Flush();
	                sw.Close();
	            }
	            catch
	            {
	            }
	        }
		  
		  public static int GetQtyLeftInTemplate(string PO)
        {

			string sql = @"Select [RESV07] as Stock,[RESV08] as Pend from [csimes_SapInterface].[dbo].[T_WORK_ORDERS] where TWO_NO = '"+ PO +"'";
			SqlDataReader rd = ToolsClass.GetDataReader(sql,csimes_SapInterface());
			int Stock = 0, Pend = 0;
        	if(rd!= null&&rd.Read())
        	{
      			Stock = Convert.ToInt32(rd.GetValue(0));
      			Pend  = Convert.ToInt32(rd.GetValue(1));
       			rd.Close();
        	}
        	if (Stock > Pend) return 0;       		
        	
        	return Pend - Stock;        	
        }
		      
		      public static int AddSNandMaterialTemplateLink(string TemplateSysID, string SN)
        {
            int returnValue = 0;
            SqlConnection sqlConne = new SqlConnection(csimes_SapInterface());
            sqlConne.Open();
            SqlTransaction sqltran = sqlConne.BeginTransaction();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConne;
            cmd.Transaction = sqltran;

            try
            {
                    string strSqlInsert = @"INSERT INTO [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK]
                        ([MODULE_SN],[PACK_MA_SYSID],[LASTUPDATETIME],[LASTUPDATEUSER])
                        VALUES('{0}','{1}','{2}','{3}')";
                    strSqlInsert = string.Format(strSqlInsert, SN, TemplateSysID, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), "SYSTEM");

                    cmd.CommandText = strSqlInsert;
                    if (cmd.ExecuteNonQuery() == 1)
                    {
                        returnValue = returnValue + 1;
                    }
             		sqltran.Commit();
                return returnValue;
            }
            catch
            {
                returnValue = 0;
                sqltran.Rollback();
                return returnValue;
            }
            finally
            {
                sqlConne.Close();
                sqltran.Dispose();
                sqlConne.Dispose();
            }
        }
//		 public static void RenewFile(string FilePath, string FileName)
//	     {
//		  		File.Move(FileName+".txt","logs\\"+FileName+"_"+DateTime.Now.ToString("yyyyMMdd") +".bac");
//		  		var myFile =  File.Create(FilePath);
//		  		myFile.Close();		  		
//	     }   
		      
		public static void RenewFile(string FilePath, string FileName)
	    {
	        	FileInfo fi = new FileInfo(FilePath);
		  		var size = fi.Length; // in bytes
		  		string MaxSize = getConfig("FileSize",false,"","config.xml");
		  		if(Convert.ToInt32(size) > Convert.ToInt32(MaxSize)*1000000)
		  		{
		  			try {
		  				File.Move(FileName+".txt","logs\\"+FileName+"_"+DateTime.Now.ToString("yyyyMMdd-hhmmss") +".bac");
			  			var myFile =  File.Create(FilePath);
			  			myFile.Close();
		  				} 
		  			catch (Exception e) 
		  			{
		  				SendEmail(e.Message);		  				
		  			}
		  		
		  		}
	    }     
		 public static bool CheckRenewNeeded()
        {
        	DateTime dt = DateTime.Now;
        	if(dt.Day == 1)
        		return true;
        	else 
        		return false;
        }
		   
		public static void SendEmail(string body)
        {
           	
        	try
            {
        		string host = getConfig("smtphost",false,"","Config.xml");
        		string port =  getConfig("smtpport",false,"","Config.xml");
        		string MailFrom = getConfig("EmailFrom",false,"","Config.xml");
        		string MailTo = getConfig("EmailTo",false,"","Config.xml");
        		string subject = getConfig("Subject",false,"","Config.xml");
        		string userNM = getConfig("userNM",false,"","Config.xml");
        		string PsW = getConfig("PassWord",false,"","Config.xml");
        		string[] Receipts = MailTo.Split(';');
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(host);

                mail.From = new MailAddress(MailFrom);
                foreach (string r in Receipts)
                	mail.To.Add(r);
                mail.Subject = subject;
                mail.Body = body;
                SmtpServer.Port = Convert.ToInt16(port);   
               	SmtpServer.Credentials = new System.Net.NetworkCredential(userNM, PsW);          
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
              
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }     
		public static String GetTimestamp(DateTime value)
        {
            return value.ToString("dd'/'MM'/'yy HH:mm:ss:ffff");
        }
	}
}
