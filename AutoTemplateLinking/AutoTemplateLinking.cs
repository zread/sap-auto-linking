﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 6/3/2016
 * Time: 10:14 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ServiceProcess;
using System.IO;
using System.Timers;
using System.Threading;
using System.Data;
using System.Data.SqlClient;

	
namespace AutoTemplateLinking
{
	public class AutoTemplateLinking : ServiceBase
	{
		public const string MyServiceName = "AutoTemplateLinking";
		private System.Timers.Timer timer1 = null;
		private static string CentrDB = ToolsClass.CentralizedDatabase();
		private static string CSIDB = ToolsClass.csimes_SapInterface();
		private static int Delay = Convert.ToInt16(ToolsClass.getConfig("Timer",false,"","Config.xml"));
		
		public AutoTemplateLinking()
		{
			InitializeComponent();
			
		}
		
		private void InitializeComponent()
		{
			this.ServiceName = MyServiceName;
		}
		
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			// TODO: Add cleanup code here (if required)
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// Start this service.
		/// </summary>
		protected override void OnStart(string[] args)
		{
			// TODO: Add start code here (if required) to start your service.
			Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
			ToolsClass.SendEmail("service started!");
			ToolsClass.WriteToLog("service started","");            
            timer1 = new System.Timers.Timer();
            this.timer1.Interval = 1000* (Delay);            // 1 seconds
            ToolsClass.WriteToLog("Delay at: "+Delay.ToString()+"s","");
            ToolsClass.WriteToLog("Running at: "+AppDomain.CurrentDomain.BaseDirectory.ToString(),"");
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_Tick);
            timer1.Enabled = true;
		}
		
		void timer1_Tick(object sender, EventArgs e)
		{
  
//			if(ToolsClass.CheckRenewNeeded())
//			{
//				ToolsClass.RenewFile(AppDomain.CurrentDomain.BaseDirectory + "\\Log.txt","Log");		
//				ToolsClass.RenewFile(AppDomain.CurrentDomain.BaseDirectory + "\\ErrorLog.txt","ErrorLog");
//			}
				
				ToolsClass.RenewFile(AppDomain.CurrentDomain.BaseDirectory + "\\Log.txt","Log");
				ToolsClass.RenewFile(AppDomain.CurrentDomain.BaseDirectory + "\\ErrorLog.txt","ErrorLog");
//				ToolsClass.WriteToLog("After Log File Renew","");
			
			string sql = @"select sn from [LabelPrintDB].[dbo].Product where Process = 'T3' and ProcessDateTime > GETDATE()-1 and SN not in (
 						 select MODULE_SN from [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK] with (nolock) where [LASTUPDATETIME] > GETDATE()-1)";
			DataTable dt = ToolsClass.PullData(sql);
			foreach (DataRow row in dt.Rows) {
				if(SNnotInFormat(row[0].ToString()))
				{
					ToolsClass.WriteErrorLog(row[0].ToString(),"","Serial number wrong format.");
					continue;
				}
				LinkToTemplate(row[0].ToString());
			}			
		}
		
		void LinkToTemplate(string SN)
		{
			string POsequence = "";
			string Line = "";
			string ParentPo = "";
			string syr = SN.Substring(1,2);
			string templateName = "";
			string tempSYSID = "";
			
			if(InReworkOrder(SN))
			{
				ToolsClass.WriteErrorLog(SN,"","Exist in ReworkOrder");
				return;
			}
			
			
			string tempPo = GetPONumber(SN);
			
			POsequence = tempPo.Substring(6,3);
			Line = tempPo.Substring(1,1);
			
			ParentPo = "C"+Line+syr+"__"+POsequence;
			if(ExistTracebility(ParentPo))
			{
				ToolsClass.WriteErrorLog(SN,"","Exist in Tracebility");
				return;
			}
			string sql = @"select distinct orderno from [TSAP_WORKORDER_SALES_REQUEST] where resv05 like '{0}' and LEN(OrderNo) = 10 ";
			sql =string.Format(sql,ParentPo);
			DataTable dt = ToolsClass.PullData(sql,CSIDB);
			if(dt == null || dt.Rows.Count < 1)
			{
				ToolsClass.SendEmail(SN+ " Production order not download!");
				ToolsClass.WriteErrorLog(SN,templateName,"Production order not download");
				sql = @" insert into [AutoLinkTracebility] values('{0}','Production order not download', getdate(), 1)";
				sql = string.Format(sql,ParentPo);
				ToolsClass.PutsData(sql);
				return;
			}
			string[] ChildPo = new string[dt.Rows.Count];
			int i = 0;
			foreach (DataRow row in dt.Rows) {
				ChildPo[i] = row[0].ToString();
				i++;
			}
			sql = @"  select MaterialTemplateName,OrderNo,SYSID from
(select ROW_NUMBER()over(partition by orderno order by operatorDate ) as r, orderno,MaterialTemplateName,SYSID,operatorDate from [T_PACK_MATERIAL_TEMPLATE] where  OrderNo in ('{0}')
)a where r = 1 order by OperatorDate";
			sql = string.Format(sql, string.Join("','", ChildPo));
			dt.Clear();
			dt = ToolsClass.PullData(sql,CentrDB);

// Required for multiple child production order under each mother order			
//			foreach (DataRow row in dt.Rows) {
//				int Qty = ToolsClass.GetQtyLeftInTemplate(row[1].ToString());
//				if(Qty != 0)
//				{
//					templateName = row[0].ToString();
//					tempSYSID = row[2].ToString();
//					break;
//				}				
//			}
			
			if(dt!=null && dt.Rows.Count > 0)
			{
				templateName = dt.Rows[0][0].ToString();
				tempSYSID = dt.Rows[0][2].ToString();
				
			}
//			
			if(string.IsNullOrEmpty(tempSYSID) || tempSYSID == "")
			{				
				ToolsClass.SendEmail(SN + " Template not found!");
				ToolsClass.WriteErrorLog(SN,templateName,"Template not found");
				sql = @" insert into [AutoLinkTracebility] values('{0}','Template not found', getdate(), 1)";
				sql = string.Format(sql,ParentPo);
				ToolsClass.PutsData(sql);
				return;
			}
			
			ToolsClass.WriteToLog(SN,templateName);
			int rev = ToolsClass.AddSNandMaterialTemplateLink(tempSYSID,SN);			
		}
	
		
		
		private bool InReworkOrder(string SN)
		{
			string sql = @"select * from T_REG_MODULE_REWORK WHERE MODULE_SN = '{0}'";
			sql  = string.Format(sql,SN);
			SqlDataReader rd = ToolsClass.GetDataReader(sql);
			if(rd.Read() && rd != null)
				return true;
			return false;
			
		}
		private bool ExistTracebility(string Po)
		{
			string sql = @"select * from [AutoLinkTracebility] where PO like '%"+Po+"%' and Status = 1";
			SqlDataReader rd = ToolsClass.GetDataReader(sql);
			if(rd.Read() && rd != null)
				return true;
			return false;
		}
		
		private bool SNnotInFormat(string SN)
		{
			bool ret= false;
			if(SN.Length != 14)
				ret = true;
			return ret;
		}
		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop()
		{
			// TODO: Add tear-down code here (if required) to stop your service.
			timer1.Enabled = false;
			ToolsClass.SendEmail("Service has stopped running");
            ToolsClass.WriteToLog("Service has stopped running","");
		}
		
		#region Sn number change
		    private string GetPONumber(string SN)
        {
        	if (string.IsNullOrEmpty(SN)) {
        		ToolsClass.WriteErrorLog("Serial number: "+SN+" does not have a matched PO, please check!","","");
        		return "";
        	}
        	
        	string sql = @"select po from PO_Modification where InterID = (select MAX(InterID) from PO_Modification where sn = '"+SN+"')";
        	string sequence = "";
        	string Line = "";
        	
        	string syr = SN.Substring(1,2);
        	string sMonth = SN.Substring(3,2);
        	
        	DataTable dt = ToolsClass.PullData(sql);
        	
        	if(dt!=null && dt.Rows.Count > 0)
        	{
        		sequence = dt.Rows[0][0].ToString().Substring(6,3);
		        Line = dt.Rows[0][0].ToString().Substring(1,1);
        	}
        	else
        	{
        		sequence = SN.Substring(7,3);
	        	int lineNumber = Convert.ToInt32(SN.Substring(5,2));
	        
	        
	        	//Changeover on 2016/10/01
	        	if(Convert.ToInt16(syr) > 16 || Convert.ToInt16(sMonth) > 9)
	        	{
		        	switch (lineNumber) {
		        			case 40:
		        					Line = "A";
		        					break;
		        				case 41:
		        					Line = "A";
		        					break;
		        				case 42:
		        					Line = "B";
		        					break;
		        				case 43:
		        					Line = "B";
		        					break;
		        				case 44:
		        					Line = "C";
		        					break;
		        				case 45:
		        					return GetPONumber(getOriginalSn(SN));
		        					break;
		        	}
	        	}
	        	else
	        	{
		        	if(lineNumber > 9 && lineNumber < 20 )
		        		Line = "A";
		        	else if(lineNumber > 19 && lineNumber < 30 )
		        		Line = "B";
		        	else if(lineNumber > 29 && lineNumber < 40 )
		        		Line = "C";
		        	else if(lineNumber > 39 && lineNumber < 50 )
		        		Line = "D";
	        	}
        	}
        	
        	return "C"+Line+syr+sMonth+sequence;
        	
        	
        }
        
        private string getOriginalSn(string SN)
        {
        	string sql = @" select DuplicatedSN from SapDuplicationSN where NewSN = '{0}'";
	        		sql = string.Format(sql,SN);
	        		DataTable OriginalSn = ToolsClass.PullData(sql);
	        		if (OriginalSn != null && OriginalSn.Rows.Count > 0 ) {
	        			return OriginalSn.Rows[0][0].ToString();
	        		}
	        		return "";
        }
		
		#endregion
	}
}
